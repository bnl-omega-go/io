package io

import (
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"math"

	"gitlab.cern.ch/bnl-omega-go/io/writers"
)

// ---------------- Reader ------------------------------//
type WFReader struct {
	io.Reader
}

type Waveform struct {
	X, Y  []float64
	name  string
	event int
}

func Float64frombytes(bytes []byte) float64 {
	bits := binary.LittleEndian.Uint64(bytes)
	float := math.Float64frombits(bits)
	return float
}

func Float64bytes(float float64) []byte {
	buf := make([]byte, 8)
	bits := math.Float64bits(float)
	binary.LittleEndian.PutUint64(buf, bits)
	return buf
}

func ReadWaveform(w io.Reader) (t, value []float64) {

	x_values := make([]float64, 0)
	y_values := make([]float64, 0)

	for {
		buf := make([]byte, 16)

		_, err := w.Read(buf)
		buft := (buf)[0:8]
		bufv := (buf)[8:16]

		t := Float64frombytes(buft)
		value := -1 * Float64frombytes(bufv)
		if err == io.EOF {
			break
		}
		x_values = append(x_values, t)
		y_values = append(y_values, value)

	}
	return x_values, y_values
}

//---------------- Writer ------------------------------//

type WFWriter struct {
	io.Writer
}

func WriteWaveform(w io.Writer, deltat, wf []float64) {

	for i, v := range wf {
		_, err := w.Write([]byte(fmt.Sprintf("%06.6e , %06.6f \n", deltat[i], v)))
		if err != nil {
			log.Fatal("error writing waveform!")
			break
		}
	}
}

func WriteWaveformToCSV(Xlabel, Ylabel, Title, filename string, deltat, wf []float64) {
	wr := writers.CSVWriter{}
	gr := writers.Graph{Name: Title, X: deltat, Y: wf, Title: Title, Xlabel: Xlabel, Ylabel: Ylabel}
	wr.Write(filename, gr)
}
func WriteWaveformToPNG(Xlabel, Ylabel, Title, filename string, deltat, wf []float64) {
	wr := writers.PlotWriter{}
	gr := writers.Graph{Name: Title, X: deltat, Y: wf, Title: Title, Xlabel: Xlabel, Ylabel: Ylabel}
	wr.Write(filename, gr)

}
func WriteWaveformToROOT(Xlabel, Ylabel, Title, filename string, deltat, wf []float64) {
	wr := writers.ROOTWriter{}
	wr.Init(filename)
	gr := writers.Graph{Name: "IV_Curve", X: deltat, Y: wf, Title: Title, Xlabel: Xlabel, Ylabel: Ylabel}
	wr.WriteGraph(gr)
	wr.Close()
}
