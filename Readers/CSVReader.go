package readers

import (
	"bytes"
	"encoding/csv"
	"io"
	"log"
	"strconv"
)

type CSVReader struct {
	R csv.Reader
}

func (r *CSVReader) Read(buf []byte) (n int, err error) {
	data, err := r.R.Read()

	if len(data) == 0 {
		return 0, io.EOF
	}

	t, _ := strconv.ParseFloat(data[3], 64)
	value, _ := strconv.ParseFloat(data[4], 64)

	var obuf bytes.Buffer
	var err2 error
	_, err = obuf.Write(Float64bytes(t))
	_, err2 = obuf.Write(Float64bytes(value))

	if err != nil || err2 != nil {
		log.Fatal(err, err2)
	}

	n = copy(buf, obuf.Bytes())

	return
}
