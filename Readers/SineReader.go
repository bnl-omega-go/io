package readers

import (
	"bytes"
	"io"
	"log"
	"math"
)

type SineReader struct {
	*WFReader
	Omega float64
	N     int
	cnt   int
}

// Read data from random number generator to make WF
func (w *SineReader) Read(buf []byte) (n int, err error) {

	var obuf bytes.Buffer
	_, err = obuf.Write(Float64bytes(float64(w.cnt)))

	if err != nil {
		log.Fatal(err)
	}
	_, err = obuf.Write(Float64bytes(math.Sin(float64(w.cnt) * w.Omega)))

	if err != nil {
		log.Fatal(err)
	}

	w.cnt++

	n = copy(buf, obuf.Bytes())

	if w.cnt < w.N {
		return n, nil
	} else {
		return n, io.EOF
	}
}
