package readers

import (
	"bytes"
	"io"
	"log"
	"math/rand"
)

type RndReader struct {
	*WFReader
	N   int
	cnt int
	R   *rand.Rand
}

// Read data from random number generator to make WF
func (w *RndReader) Read(buf []byte) (n int, err error) {

	t := float64(w.cnt)
	value := w.R.NormFloat64()

	var obuf bytes.Buffer
	_, err = obuf.Write(Float64bytes(t))

	if err != nil {
		log.Fatal(err)
	}
	_, err = obuf.Write(Float64bytes(value))

	if err != nil {
		log.Fatal(err)
	}

	w.cnt++

	n = copy(buf, obuf.Bytes())

	if w.cnt < w.N {
		return n, nil
	} else {
		return n, io.EOF
	}
}
