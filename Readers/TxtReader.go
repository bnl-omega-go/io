package readers

import (
	"bufio"
	"bytes"
	"io"
	"log"
	"strconv"
	"strings"
)

type TxtReader struct {
	R *WFReader
	S *bufio.Scanner
}

// Read data from random number generator to make WF
func (w *TxtReader) Read(buf []byte) (n int, err error) {

	success := w.S.Scan()
	text := w.S.Text()
	if !success {
		// False on error or EOF. Check error
		err = w.S.Err()
		if err == nil {
			log.Println("Scan completed and reached EOF")
			err = io.EOF
			n = 0
			return
		} else {
			log.Fatal(err)
		}
	}
	words := strings.Split(text, " ")
	t, err := strconv.ParseFloat(words[0], 64)
	value, err2 := strconv.ParseFloat(words[1], 64)

	if err != nil || err2 != nil {
		log.Fatal(err, err2)
	}

	var obuf bytes.Buffer
	_, err = obuf.Write(Float64bytes(t))
	_, err2 = obuf.Write(Float64bytes(value))

	if err != nil || err2 != nil {
		log.Fatal(err, err2)
	}

	n = copy(buf, obuf.Bytes())

	return n, err
}
