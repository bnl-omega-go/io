package writers

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"log"
	"os"
)

type CSVWriter struct {
	B  *bufio.Writer
	F  *os.File
	wr *csv.Writer
}

func (w *CSVWriter) Write(filename string, gr Graph) error {

	csvFile, err := os.Create(filename)
	defer csvFile.Close()

	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}

	w.wr = csv.NewWriter(csvFile)

	w.wr.Write([]string{gr.Xlabel, gr.Ylabel})

	for i, _ := range gr.X {
		point := []string{fmt.Sprintf("%v", gr.X[i]), fmt.Sprintf("%v", gr.Y[i])}
		err = w.wr.Write(point)
		if err != nil {
			log.Println(err)
			return err
		}
	}

	w.wr.Flush()

	return nil
}
