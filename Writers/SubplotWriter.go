package writers

import (
	"image/color"
	"io"
	"log"
	"math"
	"os"
	"strconv"
	"strings"

	"go-hep.org/x/hep/hplot"
	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/draw"
	"gonum.org/v1/plot/vg/vgimg"
)

type SubplotWriter struct {
	io.Reader
	x1, y1, y2                               []float64
	Xlabel, Ylabel, Ylabel2, Title, Filename string
}

func (w *SubplotWriter) Write(p []byte) (n int, err error) {
	// Og string
	theline := string(p)
	theline = strings.TrimRight(theline, "\r\n")

	words := strings.Split(theline, " ")
	x1, err := strconv.ParseFloat(words[0], 64)
	y1, err2 := strconv.ParseFloat(words[1], 64)
	y2, err3 := strconv.ParseFloat(words[2], 64)

	if err != nil || err2 != nil || err != nil {
		log.Fatal(err, err2, err3)
	}

	w.x1 = append(w.x1, x1)
	w.y1 = append(w.y1, y1)
	w.y2 = append(w.y2, y2)

	return len(theline), nil
}

func (w *SubplotWriter) Close() {

	const (
		width  = 20 * vg.Centimeter
		height = 1.3 * width / math.Phi
	)

	p := hplot.New()
	p.Title.Text = w.Title
	p.Y.Label.Text = w.Ylabel
	p.Y.Min, p.Y.Max = minmax(w.y1)
	p.X.Min, p.X.Max = minmax(w.x1)

	data := hplot.ZipXY(w.x1, w.y1)
	s := hplot.NewS2D(data)

	s.Color = color.RGBA{B: 123, A: 255}
	s.Shape = draw.CircleGlyph{}

	p.Add(s)
	p.Add(hplot.NewGrid())

	p2 := hplot.New()
	p2.X.Label.Text = w.Xlabel
	p2.Y.Label.Text = w.Ylabel2

	p2.Y.Min, p2.Y.Max = minmax(w.y2)
	p2.X.Min, p2.X.Max = minmax(w.x1)

	data2 := hplot.ZipXY(w.x1, w.y2)
	s2 := hplot.NewS2D(data2)

	s2.Color = color.RGBA{B: 123, A: 255}
	s2.Shape = draw.CircleGlyph{}

	p2.Add(s2)
	p2.Add(hplot.NewGrid())

	c := vgimg.PngCanvas{Canvas: vgimg.New(width, height)}
	dc := draw.New(c)

	//p.Draw(dc)
	sub := draw.Canvas{
		Canvas: dc,
		Rectangle: vg.Rectangle{
			Min: vg.Point{X: 0.0 * width, Y: 0.35 * height},
			Max: vg.Point{X: 1.00 * width, Y: 1.00 * height},
		},
	}

	sub2 := draw.Canvas{
		Canvas: dc,
		Rectangle: vg.Rectangle{
			Min: vg.Point{X: 0.0 * width, Y: 0.0 * height},
			Max: vg.Point{X: 1.00 * width, Y: 0.30 * height},
		},
	}
	p.Draw(sub)
	p2.Draw(sub2)

	f, err := os.Create(w.Filename)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	defer f.Close()
	_, err = c.WriteTo(f)
	if err != nil {
		log.Fatal(err)
	}
	err = f.Close()
	if err != nil {
		log.Fatal(err)
	}
}
