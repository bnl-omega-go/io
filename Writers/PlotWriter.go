package writers

import (
	"image/color"
	"io"
	"log"

	"go-hep.org/x/hep/hplot"
	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/draw"
)

type PlotWriter struct {
	F                               io.Writer
	x, y                            []float64
	Xlabel, Ylabel, Title, Filename string
}

func (w *PlotWriter) Write(filename string, gr Graph) error {

	for i, _ := range gr.X {
		w.x = append(w.x, gr.X[i])
		w.y = append(w.y, gr.Y[i])
	}
	w.Title = gr.Title
	w.Xlabel = gr.Xlabel
	w.Ylabel = gr.Ylabel
	w.Filename = filename

	w.Close()
	return nil
}

// func (w *PlotWriter) Write(p []byte) (n int, err error) {
// 	// Og string
// 	theline := string(p)
// 	theline = strings.TrimRight(theline, "\n")
// 	words := strings.Split(theline, ",")
// 	x, err := strconv.ParseFloat(words[0], 64)
// 	y, err2 := strconv.ParseFloat(words[1], 64)
// 	w.x = append(w.x, x)
// 	w.y = append(w.y, y)

// 	if err != nil || err2 != nil {
// 		log.Fatal(err, err2)
// 	}
// 	return len(theline), nil
// }

func minmax(data []float64) (float64, float64) {
	min, max := data[0], data[0]

	for _, v := range data {
		if v > max {
			max = v
		}
		if v < min {
			min = v
		}
	}
	return min, max
}

func (w *PlotWriter) Close() {
	p := hplot.New()
	p.Title.Text = w.Title
	p.X.Label.Text = w.Xlabel
	p.Y.Label.Text = w.Ylabel
	p.Y.Min, p.Y.Max = minmax(w.y)
	p.X.Min, p.X.Max = minmax(w.x)

	data := hplot.ZipXY(w.x, w.y)
	s := hplot.NewS2D(data)

	s.Color = color.RGBA{B: 123, A: 255}
	s.Shape = draw.CircleGlyph{}

	p.Add(s)
	p.Add(hplot.NewGrid())

	err := p.Save(40*vg.Centimeter, -1, w.Filename)
	if err != nil {
		log.Fatal(err)
	}
}
