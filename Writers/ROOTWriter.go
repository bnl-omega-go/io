package writers

import (
	"log"

	"go-hep.org/x/hep/groot"
	"go-hep.org/x/hep/groot/rhist"
	"go-hep.org/x/hep/hbook"
)

type ROOTWriter struct {
	File *groot.File
}

func (w *ROOTWriter) Init(filename string) {
	var err error
	w.File, err = groot.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
}

func (w *ROOTWriter) WriteGraph(gr Graph) {

	s := hbook.NewS2D()

	for i, v := range gr.X {
		s.Fill(hbook.Point2D{X: v, Y: gr.Y[i]})
	}
	annotation := s.Annotation()
	annotation["title"] = gr.Title
	rgraph := rhist.NewGraphFrom(s)
	w.File.Put(gr.Name, rgraph)

}

func (w *ROOTWriter) Write(Xlabel, Ylabel, Title string, deltat, wf []float64) {
	gr := Graph{Name: Title, X: deltat, Y: wf, Title: Title, Xlabel: Xlabel, Ylabel: Ylabel}
	w.WriteGraph(gr)

}

func (w *ROOTWriter) Close() error {
	log.Printf("Closing file %v", w.File.Name())
	return w.File.Close()
}
