package writers

type Graph struct {
	X, Y                        []float64
	Title, Xlabel, Ylabel, Name string
}
